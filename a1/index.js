
//3
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json())
.then(data => {
    console.log(data)
})
//4



//5 & 6
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then( data => data.json())
.then( data => {
    console.log(data);
    console.log(`The item "delectus aut autem" on the list has a status of false`)
})

//7
fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: false,
        id: 201,
        title:`create to do list item`,
        userId: 1
    })
})
.then(data => data.json())
.then(data => {
    console.log(data)
})

//8
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        completed: true,
        id: 101,
        title: "update to do list item",
        userId:1
    })
})
.then(data => data.json())
.then(data => {
    console.log(data)
})

//9
fetch(`https://jsonplaceholder.typicode.com/todos/4`, {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
    dateCompleted: "Pending",
    description: "To update my to do list item with a different data structure",
    id: 1,
    status: "pending",
    title: "updated to do list item",
    userId: 1
    })
})
.then( data => data.json())
.then( data => {
    console.log(data)
})
//10 & 11
fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body:JSON.stringify({
        completed: false,
        dateCompleted: "07/09/21",
        id: 1,
        status: "complete",
        title: "delectus aut autem",
        userId: 1
    })
})
//12
fetch(`https://jsonplaceholder.typicode.com/todos2`, {
    method: "DELETE"
})