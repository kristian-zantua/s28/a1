//GET METHOD

//console.log(fetch(`https://jsonplaceholder.typicode.com/posts`))


// GET METHOD


//fetch(`https://jsonplaceholder.typicode.com/posts`)
//.then( data => data.json())
//.then( data => {

//    data.forEach(element =>{
//        console.log(element)
//    })
//})


//GET METHOD for a single document

// fetch(`https://jsonplaceholder.typicode.com/posts/1`)
// .then(response => response.json())
// .then(response => {
//     console.log(response)
// })

//POST METHOD
// fetch(`https://jsonplaceholder.typicode.com/posts`, {
//     method: "POST",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body: JSON.stringify({
//             title: "New Post",
//             body: "Hello World",
//             userId: 1
//     })
// })
// .then( response => response.json())
// .then( response =>{
//     console.log(response)
// })

//PUT METHOD
// fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
//     method: "PUT",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body:JSON.stringify({
//         title: "Updated Post",
//          body: "Hello Updated post",
//         userId: 1
//     })
// })
// .then(res => res.json())
// .then (res =>{
//     console.log(res)
// })


//PATCH METHOD
    //applies update to a particular document's field/s
// fetch(`https://jsonplaceholder.typicode.com/posts`, {
//     method: "PATCH",
//     headers: {
//         "Content-Type": "application/json"
//     },
//     body: JSON.stringify({
//         title: "Corrected post thru patch method"
//     })
// })
// .then( data => data.json())
// .then(data => {
//     console.log(data)
// })


// DELETE METHOD deletes a particular document
fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
    method: "DELETE"
})

//FILTERING METHOD
    //filtering data by sending a query via the URL
    //single query = endpoint?parameter=value
    //multiple query = endpoint?parameter=valueA&parameter=valueB
fetch(`https://jsonplaceholder.typicode.com/posts?userId=1`)    
.then(data => data.json())
.then)data =>{
    console.log(data)
}