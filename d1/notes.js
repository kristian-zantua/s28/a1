//What is fetch method?

    //fetch API allows you to asynchronously request for a resource (data)

//fetch method syntax:
    //fetch("URL", {options})

//Example of fetch method done in the client
fetch("http:localhost:3000", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body:JSON.stringify({
        //property:value
    })
})

//.then(cb fn()) method
    //handles the promise object
    //promise can be either a resolve or reject

//Example of fetch and then method done in the client
fetch("http:localhost:3000", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body:JSON.stringify({
        //property:value
    })
})
.then( response => response.json()) //parse the data from string to JS data type
.then( response => console.log(response))